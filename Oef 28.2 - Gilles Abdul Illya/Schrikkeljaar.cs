﻿namespace Oef_28._2___Gilles_Abdul_Illya
{
    internal class Schrikkeljaar
    {

        public static bool IsSchrikkeljaar(int jaar)
        {
            bool isSchrikkeljaar;

            if (jaar % 400 == 0)
            {
                isSchrikkeljaar = true;
            }
            else if (jaar % 4 == 0 && jaar % 100 != 0)
            {
                isSchrikkeljaar = true;
            }
            else
            {
                isSchrikkeljaar = false;
            }
            return isSchrikkeljaar;
        }
    }
}
