﻿using System.Windows;

namespace Oef_28._2___Gilles_Abdul_Illya
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //Schrikkeljaar schrikkeljaar;
        //EuroConversie euroconversie;
        //Rijksregisternummer rijksregisternummer;
        private void btnSchrikkeljaar_Click(object sender, RoutedEventArgs e)
        {
            lblBoolSchrikkeljaar.Content = Schrikkeljaar.IsSchrikkeljaar(Convert.ToInt32(txtSchrikkelControle.Text));
        }

        private void btnEuroConversie_Click(object sender, RoutedEventArgs e)
        {
            lblEuroConversie.Content = EuroConversie.ToEuro(Convert.ToDecimal(txtEuroConversie.Text)) + Environment.NewLine + EuroConversie.ToBef(Convert.ToDecimal(txtEuroConversie.Text));
        }

        private void btnRRControle_Click(object sender, RoutedEventArgs e)
        {
            lblRRControle.Content = Rijksregisternummer.IsValid(txtRRControle.Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //schrikkeljaar = new Schrikkeljaar();
            //euroconversie = new EuroConversie();
            //rijksregisternummer = new Rijksregisternummer();
        }
    }
}