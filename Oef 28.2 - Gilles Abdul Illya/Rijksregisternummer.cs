﻿namespace Oef_28._2___Gilles_Abdul_Illya
{
    internal class Rijksregisternummer
    {
        public static bool IsValid(string rrnr)
        {
            bool isGoed = false;
            int hoofdgetal = Convert.ToInt32(rrnr.Substring(0, 9));
            int controlegetal = Convert.ToInt32(rrnr.Substring(9, 2));
            int rest = hoofdgetal % 97;
            if (97 - rest == controlegetal)
            {
                isGoed = true;
            }
            return isGoed;
        }
    }
}
