﻿namespace Oef_28._2___Gilles_Abdul_Illya
{
    internal class EuroConversie
    {
        static decimal _koers = 40.3399M;

        static public decimal Koers
        {
            get { return _koers; }
            set { _koers = value; }
        }
        public static decimal ToBef(decimal amount)
        {
            return amount * Koers;
        }
        public static decimal ToEuro(decimal amount)
        {
            return amount / Koers;
        }
    }
}
